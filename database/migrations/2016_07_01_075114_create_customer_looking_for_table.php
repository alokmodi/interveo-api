<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerLookingForTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_looking_for', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned()->index();
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->enum('payment_type', ['Per Annum', 'Per Hour'])->default('Per Annum');
            $table->enum('job_type', ['Full Time', 'Part Time', 'Casual', 'Internship', 'Freelance', 'Contract'])->default('Full Time');
            $table->float('salary');
            $table->enum('preferred_location', ['Current', 'Any City', 'Any State', 'International'])->default('current');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_looking_for');
    }
}
