<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 200);
            $table->string('surname', 200);
            $table->string('email', 200)->unique();
            $table->string('password', 200);
            $table->string('nationality', 200);
            $table->string('phone', 200);
            $table->date('dob');
            $table->enum('gender', ['M', 'F'])->default('M');
            $table->enum('race', ['Latino', 'Asian', 'Black', 'White', 'Indian', 'Coloured', 'Other'])->default('other');
            $table->boolean('is_active')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
    }
}
