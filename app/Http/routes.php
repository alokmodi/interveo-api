<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function(){
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');

    Route::group(['middleware' => 'jwt.auth'], function(){

        Route::group(['prefix' => 'customer'], function() {
            Route::get('my-profile', 'CustomerController@profile');
            Route::get('my-industry', 'CustomerController@industry');
            Route::get('get-qualification', 'CustomerController@getQualification');
            Route::get('get-experience', 'CustomerController@getExperience');
            Route::get('get-other-info', 'CustomerController@getOtherInfo');
            Route::get('get-interest', 'CustomerController@interest');
            Route::get('applied-for', 'CustomerController@appliedFor');
            Route::get('get-recommend-job', 'CustomerController@getRecommendJob');
            Route::get('get-looking-for', 'CustomerController@lookingFor');
            Route::get('job-detail/{jobId}', 'CustomerController@jobDetail');
            Route::get('company-detail/{companyId}', 'CustomerController@companyDetail');
            Route::get('get-video/{videoId?}', 'CustomerController@getVideo');


            Route::post('update-profile', 'CustomerController@updateProfile');
            Route::post('search', 'CustomerController@search');
            Route::post('set-qualification', 'CustomerController@setQualification');
            Route::post('set-experience', 'CustomerController@setExperience');
            Route::post('set-other-info', 'CustomerController@setOtherInfo');
            Route::post('share', 'CustomerController@share');
            Route::post('set-looking-for', 'CustomerController@setLookingFor');
            Route::post('set-recommend-job', 'CustomerController@setRecommendJob');
            Route::post('set-video', 'CustomerController@setVideo');
            Route::post('set-interest', 'CustomerController@interest');
            Route::post('apply', 'CustomerController@apply');
        });

        Route::group(['prefix' => 'list'], function() {
            Route::get('get-country', 'ListController@country');
            Route::get('get-state/{countryCode}', 'ListController@state');
            Route::get('get-city/{stateCode}', 'ListController@city');
            Route::get('industries', 'ListController@industries');
        });
    });
});
