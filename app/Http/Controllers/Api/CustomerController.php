<?php

namespace App\Http\Controllers\Api;


use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;

class CustomerController extends Controller
{
    public function __construct(){
        $this->user = (JWTAuth::parseToken()->toUser());
        if( !$this->user ){
            die(json_encode(array(
                'error' => 'invalid request'
            )));
        }
    }

    public function profile(){
       $response = array();

       $isActive = 1;
       $result = DB::table('customers')
            ->leftJoin('customer_address', function($join) use ($isActive){
                    $join->on('customers.id', '=', 'customer_address.customer_id')
                        ->on('customer_address.is_active', '=', DB::raw($isActive));
                })
            ->select(
                'customers.name',
                'customers.email',
                'customers.surname',
                'customers.nationality',
                'customers.phone',
                'customers.dob',
                'customers.gender',
                'customers.race',
                'customer_address.country',
                'customer_address.state',
                'customer_address.city'
            )
            ->get();

        die(json_encode($result));
    }

    public function updateProfile(){

    }

    public function industry(){}

    public function getQualification(){}
    public function setQualification(){}

    public function getExperience(){}
    public function setExperience(){}

    public function getOtherInfo(){}
    public function setOtherInfo(){}

    public function getInterest(){}
    public function setInterest(){}


    public function appliedFor(){}
    public function apply(){}


    public function getRecommendJob(){}
    public function setRecommendJob(){}

    public function getVideo($videoId = null){}
    public function setVideo(){}

    public function jobDetail($jobId){}
    public function companyDetail($companyId){}
}
