<?php

namespace App\Http\Controllers\Api;


use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;

class ListController extends Controller
{
    public function __construct(){
        $this->user = (JWTAuth::parseToken()->toUser());
        if( !$this->user ){
            die(json_encode(array(
                'error' => 'invalid request'
            )));
        }
    }

    public function country(){
        $result = DB::table('countries')->get();
        die(json_encode($result));
    }

    public function state($countryCode){
        $result = DB::table('regions')
                    ->where('country_code', '=', $countryCode)
                    ->get();
        die(json_encode($result));
    }

    public function city($stateCode){
        $result = DB::table('cities')
                    ->where('region', '=', $stateCode)
                    ->get();
        die(json_encode($result));
    }

    public function industries(){
        $result = DB::table('industries')->get();
        die(json_encode($result));
    }

}