<?php

namespace App\Http\Controllers\Api;

use DB;
use Validator;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;

class AuthController extends Controller
{
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Login a user
     *
     * @param  Request $request
     * @return JSON
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }

    /**
     * Refresh the token
     *
     * @return JSON
     */
    public function refresh()
    {
        $newToken = JWTAuth::parseToken()->refresh();
        return response()->json(compact('newToken'));
    }

    /**
     * Register a User
     *
     * @param  Request $request
     * @return JSON
     */
    public function register(CustomerRequest $request)
    {
            $data = $request;

            DB::transaction(function() use ($data) {
                $customer = new \App\Customer();
                try{
                   $customerId = $customer->insertGetId([
                        'name' => $data['name'],
                        'email' => $data['email'],
                        'password' => bcrypt($data['password']),
                        'nationality' => $data['nationality'],
                        'dob' => $data['dob'],
                        'gender' => $data['gender'],
                        'race' => $data['race'],
                        'is_active' => 0
                    ]);

                } catch(\PDOException $e){
                   die(json_encode(array(
                                'message' => 'Some problem occured',
                                'success' => false
                   )));
                }
            });

            die(json_encode(array(
                'message' => 'Account has been created',
                'success' => true
            )));
    }
}
