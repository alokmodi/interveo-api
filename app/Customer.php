<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    protected $table = 'customers';

    protected $hidden = ['password', 'remember_token'];
    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'nationality',
        'dob',
        'gender',
        'race',
        'password'
    ];

    /**
     * Rules
     *
     * @var array
     */
    public static $rules = [
        'name'  => 'required',
        'email'  => 'required|unique:customers',
        'nationality' => 'required',
        'dob' => 'required',
        'gender' => 'required',
        'race' => 'required',
        'password' => 'required'
    ];
}
